/* VARIABLES
===============================================================
*/
var _restUploadApi = 'http://localhost:8080/upload/api';
var _restUploadApiCallback = 'callback=?';
var _restUploadsPOST = _restUploadApi + '/uploads' + '?' + _restUploadApiCallback;
var _restUploadsDELETE = _restUploadApi + '/uploads/%1' + '?' + _restUploadApiCallback;

/* NAMESPACES
===============================================================
*/
var Models = namespace("drdynscript.appupload.models");

/* SELF EXECUTING METHOD 
===============================================================
*/
(function(){
  var App = {
    init:function(){
      this.ENTER_KEY = 13;
      this._nextUploadFilePointer = 0;
      this.cacheElements();
      this.bindEvents();
    },
    cacheElements:function(){
      this.$appUpload = $('#appUpload');
      this.$filesToUpload = $('#filesToUpload');
      this.$filesDropZone = $('#filesDropZone');
      this.$uploadsList = $('#uploadsList');
    },
    bindEvents:function(){
      this.$filesToUpload.on('change', this.createUploads); 
      this.$filesDropZone.bind({
        dragover: function (e) {
          e = e || window.event;
          e.preventDefault();
          e = e.originalEvent || e;
          e.dataTransfer.dropEffect = "copy";
          $(this).addClass('dzdragover');
          return false;
        },
        dragend: function (e) {
          e = e || window.event;
          e.preventDefault();
          $(this).removeClass('dzdragover');
          return false;
        },
        dragleave: function (e) {
          e = e || window.event;
          e.preventDefault();
          $(this).removeClass('dzdragover');
          return false;
        },
        drop: function (e) {
          e = e || window.event;
          e.preventDefault();
          e = e.originalEvent || e;
          var files = (e.files || e.dataTransfer.files);
          $(this).removeClass('dzdragover');
          App.uploadFiles(files);  
          return false;
        }
      });
      this.$uploadsList.on('click', '.destroy', this.deleteFile);
    },
    createUploads:function(e){
      App.uploadFiles(e.target.files);      
    },
    uploadFiles:function(files){
      //UPLOAD FILES
      for (var i = 0, file; file = files[i]; ++i) {
        //INCREMENT POINTER
        ++App._nextUploadFilePointer;
        //RENDER FILE
        App.renderFile(file,App._nextUploadFilePointer);
        //UPLOAD THE FILE
        App.uploadFile(file,App._nextUploadFilePointer);
      }
    },
    uploadFile:function(file, pointer){
      //CREATE FORMDATA OBJECT
      var formData = new FormData();
      //ADD FILE
      formData.append(file.name, file);
      //ADD EXTRA INFORMATION
      formData.append("author", "Philippe De Pauw - Waterschoot");

      //CREATE E NEW REQUEST
      var xhr = new XMLHttpRequest();

      //REGISTER LISTENERS
      xhr.upload.addEventListener("progress", function(evt){
        if (evt.lengthComputable) {
          var percentComplete = Math.round(evt.loaded * 100 / evt.total);
          var pb = $('#uploadsList .f' + pointer).find('progress');
          $(pb).attr('value', percentComplete);
          $(pb).html(percentComplete.toString() + '%');
          if(percentComplete == 100){
            $('#uploadsList .f' + pointer).addClass('completed');
          }
        }
        else {
          console.log('unable to compute');
        }
      }, false);
      xhr.addEventListener("load", function(evt){
        $('#uploadsList .f' + pointer).attr('data-id', evt.target.responseText);
      }, false);
      xhr.addEventListener("error", function(evt){
        console.log("There was an error attempting to upload the file.");
      }, false);
      xhr.addEventListener("abort", function(evt){
        console.log("The upload has been canceled by the user or the browser dropped the connection.");
      }, false);

      xhr.open("POST", _restUploadsPOST);
      xhr.send(formData);
    },
    renderFile:function(file, pointer){
      var htmlContent = ''
      + '<li class="f' + pointer + ' clearfix">'
      + ((file.type.indexOf('image') != -1)?'<figure class="fileThumbnail"><img src="" /></figure>':'<section class="fileThumbnail"><span>DOC</span></section>')
      + '<section class="fileInformation">'
      + '<h3>' + file.name + '</h3>'
      + '<section class="fileMeta">'
      + '<span><strong>Filesize: </strong>' + (file.size/1024).toFixed(2) + ' kB</span>' 
      + '<span><strong>Filetype: </strong>' + file.type + '</span>'
      + '<span><strong>Last modified: </strong>' +  new Date(file.lastModifiedDate).toLocaleDateString()  + '</span>'
      + '</section>'
      + '<section class="progress">'
      + '<progress class="pb" value="0" max="100">0 %</progress>'
      + '</section>'
      + '</section>'
      + '<button class="destroy"></button>'
      + '</li>';
      $('#uploadsList').prepend(htmlContent);
      //READ FILE
      if(file.type.indexOf('image') != -1)
        App.readFile(file, pointer);
    },
    readFile:function(file, pointer){
      //CREATE A FILEREADER
      var reader = new FileReader();
      reader.onstart = function(e){
      }
      reader.onprogress = function(e){
      }
      reader.onabort = function(e){
      }
      reader.onerror = function(e){
      }
      reader.onload = function(e){
        var data = e.target.result;
        $('.f' + pointer + ' .fileThumbnail img').attr('src', data);
      }
      reader.onloadend = function(e){
      }
      //READ THE FILE
      reader.readAsDataURL(file);
    },
    deleteFile:function(e){
      var id = $(e.target).parent().data('id');
      $.ajax({
        type:"DELETE",
        dataType:"jsonp",
        contentType:"application/json",
        cache:false,
        url:_restUploadsDELETE.replace('%1',id),
        beforeSend:function(xhr){
        },
        success:function(data){
          $(e.target).parent().remove();
        },
        complete:function(data){
          $(e.target).parent().remove();
        },
        error:function(xhr, status, error){
        }
      });
    }
  }
  App.init();
}(window));