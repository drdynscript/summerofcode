<?php
set_include_path(dirname(__FILE__) . '/../../vendor' . PATH_SEPARATOR . get_include_path());

require_once 'Slim/Slim.php';

// Register Slim's autoloader
\Slim\Slim::registerAutoloader();

//Register non-Slim autoloader
function customAutoLoader( $class )
{
    $file = rtrim(dirname(__FILE__), '/') . '/' . $class . '.php';
    if ( file_exists($file) ) {
        require $file;
    } else {
        return;
    }
}
spl_autoload_register('customAutoLoader');

// Create an instance of Slim
$app = new \Slim\Slim();

// Upload multiple files
$app->post('/uploads', function() use ($app){
    //GET FORMDATA VIA POST VARIABLES
    foreach($_FILES as $file){        
        $nameparts = explode('.', $file["name"]);
        $ext = end($nameparts);
        $randomName = hash("sha256",microtime(true),false).'.'.$ext;
        move_uploaded_file($file["tmp_name"], '../uploads/' . $randomName);           
        echo $randomName;
    } 
});
// Delete certain file
$app->delete('/uploads/:name', function($name) use ($app){
    $filename = '../uploads/' . $name;
    if(file_exists($filename)){
        unlink($filename);
        echo 'deleted';
    }
});

// Get all folders and files from a certain webfolder via HTTP POST
$app->post('/folders', function() use ($app){
    // Get dirobj data via request body
    $requestBody = $app->request()->getBody();
    $dirobj = json_decode($requestBody, true);
    $dir = $dirobj["filename"] . '/*';

    $json = '{';
    $json .= '"filename":"' . $dirobj["filename"] . '"';
    $json .= ',"filetype":"' . filetype($dirobj["filename"]) . '"';
    $json .= ',"files":[';
    $firstItem = true;
    foreach(glob($dir) as $file) 
    {
        if($firstItem == true){
            $firstItem = false;
        } else{
            $json .= ',';
        }
        $json .= '{';
        $json .= '"filename":"' . $file . '","filetype":"' . filetype($file) . '"';
        $json .= '}';
    }
    $json .= ']';
    $json .= '}';
    echo $json;
});

// Run RESTful service
$app->run();

// Function: Helper PDO
function getDatabaseConnection() {
    $dbhost="127.0.0.1";
    $dbuser="root";
    $dbpass="";
    $dbname="uploads";
    $db = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $db;
}

// Function: Helper for jsonCallback
function helperJSONCallback($app){
    //GET CALLBACK
    $callback = $app->request()->get('callback');
    //SET RESPONSE HEADER
    if($callback == '?' || $callback == '')
        $app->response()->header("Content-Type", "application/json");
    else
        $app->response()->header("Content-Type", "application/javascript");
    return $callback;
}